import * as type from 'constants/user';

export const isLoggedIn = (data) => ({
  type: type.IS_LOGGED_IN_SUCCESS,
  token: { ...data },
});