/* eslint-disable */
import React, { useEffect, useRef, memo } from 'react';
import { uniqBy } from 'lodash';
import { useMutation, useQuery } from '@apollo/react-hooks';
import InfiniteScroll from 'react-infinite-scroller';

import { GET_CHAT_MESSAGE } from 'graphql/query';
import { READ_MESSAGES } from 'graphql/mutation';
import { SUBSCRIBE_CHAT_NEW_MESSAGE } from 'graphql/subscriptions';
import Well from 'components/Well';
import MessageItemComponent from 'modules/chat/components/MessageItemComponent';

import css from 'styles/modules/chat/ChatMessage.scss';

const ChatMessage = ({ user, scrollElemRef, selectedChat }) => {
  const { subscribeToMore, data: messageData, fetchMore } = useQuery(GET_CHAT_MESSAGE, {
    variables: {
      chatId: selectedChat._id,
      offset: 0,
    },
    fetchPolicy: 'cache-and-network',
  });
  const [readMessages] = useMutation(READ_MESSAGES);
  const chatContentRef = useRef();

  useEffect(() => {
    if (selectedChat._id && messageData) {
      const messagesOfUser = messageData.getChatMessage.filter(m => m.sender === selectedChat.user._id);
      const isReadFalse = messagesOfUser.some(m => m.is_read === false);
      if (messagesOfUser.length >= 1 && isReadFalse) {
        readMessages({
          variables: {
            chatId: selectedChat._id,
            receiver: selectedChat.user._id,
          },
        });
      }
    }
  }, [selectedChat, messageData]);

  useEffect(() => {
    let unsubscribe;
    if (selectedChat._id && subscribeToMore) {
      unsubscribe = subscribeToMore({
        document: SUBSCRIBE_CHAT_NEW_MESSAGE,
        variables: {
          chatId: selectedChat._id,
          receiver: selectedChat.user._id,
        },
        updateQuery: (prev, { subscriptionData }) => {
          if (!prev || !subscriptionData.data) return prev;
          const { chatNewMessage } = subscriptionData.data;

          const isUniqueId = prev.getChatMessage.some((item) => item._id === chatNewMessage._id);
          if (isUniqueId) return prev;

          if (chatNewMessage.sender === selectedChat.user._id) {
            readMessages({
              variables: {
                chatId: chatNewMessage.chat_id,
                receiver: chatNewMessage.sender,
              },
            });
          }
          return { ...prev, getChatMessage: [...prev.getChatMessage, chatNewMessage] };
        },
      });
    }

    return (() => {
      return unsubscribe();
    })
  }, [selectedChat, subscribeToMore]);

  const onFetchMore = () => {
    return fetchMore({
      variables: {
        chatId: selectedChat._id,
        offset: messageData.getChatMessage.length,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!prev) return false;
        if (!fetchMoreResult) return prev;
        const uniqueMessages = uniqBy([...fetchMoreResult.getChatMessage, ...prev.getChatMessage], '_id');

        return { ...prev, getChatMessage: uniqueMessages};
      },
    });
  }

  return (
    <div ref={chatContentRef} className={css.chatMessageOverflow}>
      <ul className={css.messages}>
        {messageData && (
          <InfiniteScroll
            pageStart={0}
            loadMore={onFetchMore}
            hasMore
            isReverse
            useWindow={false}
            getScrollParent={() => chatContentRef && chatContentRef.current}
          >
            {messageData.getChatMessage.map((messageInfo) => {
                const isCurrentUser = selectedChat.user._id !== messageInfo.sender;
                const currentUser = isCurrentUser ? { ...user } : { ...selectedChat.user };
                const userLang = navigator.language || navigator.userLanguage;

                return (
                  <MessageItemComponent
                    key={messageInfo._id}
                    selectedChat={selectedChat}
                    isCurrentUser={isCurrentUser}
                    currentUser={currentUser}
                    userLang={userLang}
                    messageInfo={messageInfo}
                  />
                );
              })}
              {messageData.getChatMessage.length === 0 && <Well>No messages...</Well>}
            <li key="last" ref={scrollElemRef} />
          </InfiniteScroll>
        )}
      </ul>
    </div>
  );
};

export default memo(ChatMessage);
