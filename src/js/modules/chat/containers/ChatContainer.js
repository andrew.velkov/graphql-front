/* eslint-disable */
import React, { useState, useRef, useEffect } from 'react';
import cx from 'classnames';
import { useQuery } from '@apollo/react-hooks';
import { useDispatch } from 'react-redux';

import { GET_USER_DATA } from 'graphql/query';

import { isLoggedIn } from 'actions';
import Fetching from 'components/Fetching';
import Well from 'components/Well';
import ChatTab from 'modules/chat/containers/ChatTab';
import ChatForm from 'modules/chat/containers/ChatForm';
import ChatMessageComponent from 'modules/chat/containers/ChatMessage';
import ChatHeaderComponent from 'modules/chat/components/ChatHeaderComponent';

import css from 'styles/modules/chat/ChatContainer.scss';

const ChatContainer = () => {
  const [selectedChat, setselectedChat] = useState({});
  const {client, loading, data, error } = useQuery(GET_USER_DATA);
  const scrollElemRef = useRef(null);
  const dispatch = useDispatch();

  const addGlobalClass = (selector) => {
    document.body.classList.add(selector);
  }

  const removeGlobalClass = (selector) => {
    document.body.classList.remove(selector);
  }

  useEffect(() => {
    if (selectedChat._id) addGlobalClass('globalClassForMobile');
    return () => {
      removeGlobalClass('globalClassForMobile');
    };
  }, [selectedChat]);

  if (error) {
    client.clearStore();
    localStorage.clear();
    dispatch(isLoggedIn(false));
  }
  
  if (loading) {
    return <Fetching isFetching />;
  }

  const { user, users } = data;

  const handleMessagesByChatGroupId = (chat) => () => {
    setselectedChat(chat);
    addGlobalClass('globalClassForMobile');
  };

  const getContact = (newChatItem) => {
    setselectedChat(newChatItem);
  }

  return (
    <section className={css.chat}>
      <article className={css.chat__container}>
        <aside className={css.chat__aside}>
          <ChatTab
            user={user}
            users={users}
            selectedChat={selectedChat}
            getContact={getContact}
            handleMessagesByChatGroupId={handleMessagesByChatGroupId}
          />
        </aside>
        <section className={cx(css.chat__content, css.chat__content_mobile)}>
          {selectedChat._id && (
            <>
              <section className={css.chat__history}>
                <ChatHeaderComponent
                  user={user}
                  selectedChat={selectedChat}
                />
                <ChatMessageComponent
                  user={user}
                  scrollElemRef={scrollElemRef}
                  selectedChat={selectedChat}
                />
                <ChatForm
                  selectedChat={selectedChat}
                  scrollElemRef={scrollElemRef}
                />
              </section>
            </>
          )}

          {!selectedChat._id && (
            <Well>
              Select who you would like to write to
            </Well>
          )}
        </section>
      </article>
    </section>
  );
};

export default ChatContainer;
