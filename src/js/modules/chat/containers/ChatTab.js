import React, { useState, useRef } from 'react';
import cx from 'classnames';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Switch, InputBase, Divider, Paper, Tabs, Tab, Icon, IconButton } from '@material-ui/core';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useDispatch } from 'react-redux';

import { GET_CHAT_GROUP } from 'graphql/query';
import { CREATE_CHAT } from 'graphql/mutation';

import { isLoggedIn } from 'actions';
import Fetching from 'components/Fetching';
import ChatTabDialogComponent from 'modules/chat/components/ChatTabDialogComponent';
import ChatTabSettingsComponent from 'modules/chat/components/ChatTabSettingsComponent';
import ChatTabContactsComponent from 'modules/chat/components/ChatTabContactsComponent';

import css from 'styles/modules/chat/ChatTab.scss';

const useStyles = makeStyles((theme) => ({
  rootFooter: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
  },
  rootSearch: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
}

const a11yProps = (index) => {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const ChatTab = ({ user, users, selectedChat, handleMessagesByChatGroupId, getContact }) => {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = useState(1);
  const [contact, setContactForLoading] = useState('');
  const [userFilter, setUserFilter] = useState('');
  const [isOnlineCheck, setIsOnlineCheck] = useState(false);
  const { client, loading, data, error, refetch } = useQuery(GET_CHAT_GROUP, {
    variables: { offset: 0 },
    // pollInterval: 20000,
  });
  const [createChat, { loading: createChatLoading }] = useMutation(CREATE_CHAT);
  const dispatch = useDispatch();
  const chatGroupWrapRef = useRef();

  if (loading) {
    return <Fetching isFetching />;
  }

  if (error) {
    return <span>Error</span>;
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
    if (newValue === 2) getContact({});
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handleChangeFilter = (e) => {
    setUserFilter(e.target.value);
  }

  const handleChangeIsOnline = (e) => {
    setIsOnlineCheck(e.target.checked);
  }

  const handleAddContact = (contactItem) => (e) => {
    e.preventDefault();
    setContactForLoading(contactItem);

    const newChatItem = data.getChatGroup.find((item) => item.user._id === contactItem.id);
    if (newChatItem) {
      getContact(newChatItem);
      // setValue(1);
      return false;
    }

    createChat({
      variables: { receiver: contactItem.id },
    }).then((res) => {
      if (res) {
        setValue(1);
        refetch();
        if (newChatItem) getContact(newChatItem);
      }
    });
    return true;
  }

  const usersFiltered = users.filter((userItem) => (
    (userItem.first_name.toLowerCase().includes(userFilter.toLowerCase())
      || userItem.last_name.toLowerCase().includes(userFilter.toLowerCase()))
        && userItem.id !== user.id),
  );

  const handleLogout = () => {
    // location.href = '/login'
    localStorage.clear();
    client.clearStore();
    dispatch(isLoggedIn(false));
  };

  return (
    <section className={css.chatGroupContainer}>
      {value <= 1 && (
        <header className={css.chatGroupHeader}>
          <Paper component="form" className={classes.rootSearch}>
            <IconButton className={classes.iconButton} aria-label="menu">
              <Icon>search</Icon>
            </IconButton>
            <InputBase
              className={classes.input}
              placeholder="Search..."
              value={userFilter}
              onChange={handleChangeFilter}
              inputProps={{ 'aria-label': 'search...' }}
            />
            {value !== 0 && (
              <>
                <Divider className={classes.divider} orientation="vertical" />
                <Switch
                  size="small"
                  color="primary"
                  checked={isOnlineCheck}
                  disabled={data.getChatGroup.length === 0}
                  onChange={handleChangeIsOnline}
                />
              </>
            )}
          </Paper>
        </header>
      )}

      <section
        ref={chatGroupWrapRef}
        className={cx(css.chatGroupWrap, {
          [css.chatGroupWrap_settings]: value === 2,
        })}
      >
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value}
          style={{ width: '100%' }}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <ChatTabContactsComponent
              data={data}
              usersFiltered={usersFiltered}
              handleAddContact={handleAddContact}
              createChatLoading={createChatLoading}
              contact={contact}
            />
          </TabPanel>

          <TabPanel value={value} index={1} dir={theme.direction}>
            <ChatTabDialogComponent
              data={data}
              user={user}
              userFilter={userFilter}
              isOnlineCheck={isOnlineCheck}
              selectedChat={selectedChat}
              refetch={refetch}
              handleMessagesByChatGroupId={handleMessagesByChatGroupId}
            />
          </TabPanel>

          <TabPanel value={value} index={2} dir={theme.direction}>
            <ChatTabSettingsComponent
              data={data}
              user={user}
              handleAddContact={handleAddContact}
              handleLogout={handleLogout}
              createChatLoading={createChatLoading}
              contact={contact}
            />
          </TabPanel>
        </SwipeableViews>
      </section>

      <footer className={css.chatGroupFooter}>
        <Paper square className={classes.rootFooter}>
          <Tabs
            value={value}
            onChange={handleChange}
            variant="fullWidth"
            indicatorColor="secondary"
            textColor="secondary"
            aria-label="icon label tabs example"
          >
            <Tab {...a11yProps(0)} icon={<Icon>group_add</Icon>} label={<i>Contacts</i>} />
            <Tab {...a11yProps(1)} icon={<Icon>chat</Icon>} label={<i>Chats</i>} />
            <Tab {...a11yProps(2)} icon={<Icon>settings</Icon>} label={<i>Settings</i>} />
          </Tabs>
        </Paper>
      </footer>
    </section>
  );
};

export default ChatTab;
