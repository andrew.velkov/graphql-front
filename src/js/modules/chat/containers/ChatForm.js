import React, { useState, useEffect } from 'react';
import { TextField, Icon, IconButton } from '@material-ui/core';
import { useMutation, useSubscription } from '@apollo/react-hooks';

import { CREATE_CHAT_MESSAGE, USER_TYPPING } from 'graphql/mutation';
import { SUBSCRIBE_CHAT_NEW_MESSAGE } from 'graphql/subscriptions';

import css from 'styles/modules/chat/chatForm.scss';

const ring = new Audio('//api.avoo.com.ua/uploads/chat/gemessage.mp3');
ring.volume = 0.3;

const chatForm = ({ scrollElemRef, selectedChat }) => {
  const [messageInput, setMessageInput] = useState('');
  const [timer, setTimer] = useState(null);
  const [createChatMessage] = useMutation(CREATE_CHAT_MESSAGE);
  const [userTyping] = useMutation(USER_TYPPING);
  const { data } = useSubscription(SUBSCRIBE_CHAT_NEW_MESSAGE, {
    variables: { chatId: selectedChat._id, variables: selectedChat.user._id },
  });

  useEffect(() => {
    if ((data && data.chatNewMessage._id) && (scrollElemRef && scrollElemRef.current)) {
      scrollElemRef.current.scrollIntoView({ block: 'end', behavior: 'smooth' });
    }
  }, [data, scrollElemRef]);

  const handleChangeChatMessage = (e) => {
    const { value } = e.target;

    setMessageInput(value);
    userTyping({ variables: { receiver: selectedChat.user._id } });
    const changeId = () => userTyping({ variables: { receiver: '' } });

    clearTimeout(timer);
    setTimer(setTimeout(changeId, 2000));
  };

  const handleMessage = () => {
    if (messageInput.trim() === '') {
      return false;
    }
    createChatMessage({
      variables: {
        chatId: selectedChat._id,
        receiver: selectedChat.user._id,
        message: messageInput,
      },
    });

    setMessageInput('');
    ring.play();
    return true;
  }

  const handleKeyPressChatMessage = (e) => {
    if (e.key === 'Enter') {
      if (!e.shiftKey) {
        e.preventDefault();
        handleMessage();
      }
    }
  }

  return (
    <div className={css.chatForm}>
      <TextField
        label="Message..."
        fullWidth
        placeholder="Enter Or Shift+Enter"
        variant="outlined"
        value={messageInput}
        multiline
        rowsMax={7}
        onChange={handleChangeChatMessage}
        onKeyPress={handleKeyPressChatMessage}
      />
      <div className={css.chatForm__button}>
        <IconButton color="primary" disabled={messageInput.trim() === ''} onClick={handleMessage}>
          <Icon>send</Icon>
        </IconButton>
      </div>
    </div>
  );
};

export default chatForm;
