
/* eslint-disable */
import React, { useEffect, useState } from 'react';
import { useSubscription } from '@apollo/react-hooks';
// import { Icon } from '@material-ui/core';

import { SUBSCRIBE_CHAT_NEW_MESSAGE, SUBSCRIBE_READ_MESSAGES } from 'graphql/subscriptions';
// import Fetching from 'components/Fetching';
import css from 'styles/modules/chat/Other.scss';

const ring = new Audio('//api.avoo.com.ua/uploads/chat/sound.mp3');
ring.volume = 0.2;

const UserLastMessage = ({ user, chatItem, selectedChat, refetch }) => {
  const [unread, setUnread] = useState(chatItem.unread_messages);
  // const [uncheck, setUncheck] = useState(false);
  const { data } = useSubscription(SUBSCRIBE_CHAT_NEW_MESSAGE, {
    variables: {
      chatId: chatItem._id,
      receiver: chatItem.user._id,
    },
  });
  const { data: readMessagesData } = useSubscription(SUBSCRIBE_READ_MESSAGES, {
    variables: {
      chatId: chatItem._id,
      receiver: chatItem.user._id,
    },
  });

  const readMessages = readMessagesData && readMessagesData.readMessagesSubscription.is_read === null;

  // const isNewMessageByCurrentUser = (data && data.chatNewMessage.sender === user.id);
  // const isNewMessageReadByCurrentUser = data && data.chatNewMessage.is_read;

  // const isCheckByCurrentUser = isNewMessageByCurrentUser ? isNewMessageByCurrentUser : chatItem.last_message.sender === user.id;
  // const isCheckMessByCurrentUser = isCheckByCurrentUser && chatItem.last_message.is_read;

  // console.log('data && data.chatNewMessage', data && data.chatNewMessage)

  // useEffect(() => {
  //   refetch();
  //   if (isCheckByCurrentUser) setUncheck(isCheckMessByCurrentUser);
  // }, []);

  // useEffect(() => {
  //   if (readMessages) {
  //    setUncheck(true);
  //   }
  //  }, [readMessages]);

  // useEffect(() => {
  //  if ((data && !data.chatNewMessage.is_read) && !readMessages) {
  //   setUncheck(false);
  //  }
  // }, [data, readMessages]);

  useEffect(() => {
    if ((selectedChat && selectedChat._id === chatItem._id) && readMessages) {
      setUnread(0);
      refetch();
    }
  }, [selectedChat, readMessagesData, refetch]);

  useEffect(() => {
    if ((data && data.chatNewMessage.message) && user.id === data.chatNewMessage.receiver) {
      ring.play();
      if (selectedChat._id !== data.chatNewMessage.chat_id) {
        setUnread((prev) => prev + 1);
        refetch();
      }
    }
  }, [data, ring, user, refetch]);

  return (
    <>
      {/* {isCheckByCurrentUser && (
        <Icon fontSize="small">
          {uncheck ? 'done_all' : (readMessages ? 'done_all' : 'check')}
        </Icon>
      )} */}
    {(chatItem.unread_messages >= 1 || unread >= 1) && (
      <span className={css.unreadMessages}>
        <span style={{ margin: 'auto' }}>
          {(chatItem.unread_messages >= 99 || unread >= 99) ? '99+' : (chatItem.unread_messages || (unread === 0 ? '' : unread))}
        </span>
      </span>
      )}
    </>
  );
};

export default UserLastMessage;
