/* eslint-disable */
import React from 'react';
import { gql } from 'apollo-boost';
import cx from 'classnames';
import { useSubscription } from '@apollo/react-hooks';
import { IconButton, Icon } from '@material-ui/core';

import { SUBSCRIBE_USER_TYPPING } from 'graphql/subscriptions';
import LoadingImg from 'assets/images/loading.gif';

import css from 'styles/modules/chat/ChatHeader.scss';

const ChatHeaderComponent = ({ user, selectedChat }) => {
  const { data: userTypedData } = useSubscription(SUBSCRIBE_USER_TYPPING, {
    variables: { receiver: selectedChat.user._id },
  });

  const handleBackToChatList = () => {
    document.body.classList.remove('globalClassForMobile');
  }

  return (
    <header className={css.chatHeader}> 
      <div className={css.chatHeader__wrap}>
        <IconButton size="small" className={css.chatHeader__buttonBack} onClick={handleBackToChatList}>
          <Icon>arrow_back_ios</Icon>
        </IconButton>
        {selectedChat.user.profile_img
          ? <img className={css.chatHeader__img} src={selectedChat.user.profile_img} alt="" />
          : (
            <span className={cx(css.chatHeader__img, css.chatHeader__img_none)}>
              {selectedChat.user.first_name[0]}
              {selectedChat.user.last_name[0]}
            </span>
          )}
        <h3 className={css.chatHeader__name}>
          {selectedChat.user.first_name}
          {' '}
          {selectedChat.user.last_name}
          <div>
            {userTypedData && userTypedData.userTypingSubscription === user.id
              ? (
                <div className={css.chatHeader__typing}>
                  <img src={LoadingImg} alt=""/>
                  <Icon color="secondary">edit</Icon>
                </div>
                )
              : ''
            }
          </div>
        </h3>
      </div>
    </header>
  );
};

export default ChatHeaderComponent;
