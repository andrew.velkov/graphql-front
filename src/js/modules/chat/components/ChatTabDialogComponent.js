import React from 'react';
import cx from 'classnames';
import moment from 'moment';
import { MenuItem } from '@material-ui/core';

import Well from 'components/Well';

import UserTyping from 'modules/chat/components/UserTyping';
import UserLastMessage from 'modules/chat/components/UserLastMessage';
import UserUnreadMessage from 'modules/chat/components/UserUnreadMessage';

import css from 'styles/modules/chat/ChatTabDialog.scss';

const ChatTabDialogComponent = ({
    user, userFilter, isOnlineCheck, data, selectedChat, handleMessagesByChatGroupId, refetch,
  }) => {
  // const userLang = navigator.language || navigator.userLanguage
  // .locale(userLang)

  const chats = [...data.getChatGroup];

  const getLastSeenMoment = (date) => moment(date).fromNow();
  const timesName = ['a few seconds ago'];

  const chatsFiltered = chats.filter((item) => {
    const date = getLastSeenMoment(item.user.last_seen);
    const isOnline = timesName.some((t) => date.includes(t))

    return (item.user.first_name.toLowerCase().includes(userFilter.toLowerCase())
      || item.user.last_name.toLowerCase().includes(userFilter.toLowerCase()))
        && (isOnlineCheck !== true || isOnline)
  });

  const chatsFilteredSort = chatsFiltered.sort((a, b) => (
    new Date(b.last_message.createdAt) - new Date(a.last_message.createdAt)
  ));

  return (
    <ul className={css.chatGroup}>
      {chatsFilteredSort.map((chat) => {
        const date = getLastSeenMoment(chat.user.last_seen);
        const isOnline = timesName.some((t) => date.includes(t));

        return (
          <div
            key={chat._id}
            className={cx(css.chatGroup__item, {
              [css.chatGroup__item_selected]: selectedChat && selectedChat._id === chat._id,
            })}
          >
            <MenuItem onClick={handleMessagesByChatGroupId(chat)} className={css.chatGroup__link}>
              {chat.user.profile_img
                ? <img className={css.chatGroup__img} src={chat.user.profile_img} alt="" />
                : (
                  <span className={cx(
                    css.chatGroup__img,
                    css.chatGroup__img_none,
                    css.chatGroup__imgnone,
                  )}
                  >
                    {chat.user.first_name[0]}
                    {chat.user.last_name[0]}
                  </span>
                )
              }
              <div className={css.chatGroup__name}>
                <p>
                  {chat.user.first_name}
                  {' '}
                  {chat.user.last_name}
                </p>

                <UserUnreadMessage
                  chatItem={chat}
                  refetch={refetch}
                  selectedChat={selectedChat}
                  user={user}
                />

                <UserTyping user={user} chatItem={chat}>
                  <UserLastMessage
                    chatItem={chat}
                    refetch={refetch}
                    selectedChat={selectedChat}
                    user={user}
                  />
                </UserTyping>

                <span className={cx(css.chatGroup__status, {
                    [css.chatGroup__status_online]: isOnline,
                  })}
                >
                  {isOnline ? 'online' : date}
                </span>
              </div>
            </MenuItem>
          </div>
        )},
      )}

      {chatsFiltered.length === 0 && <Well>No chats yet...</Well>}
    </ul>
  )
};

export default ChatTabDialogComponent;
