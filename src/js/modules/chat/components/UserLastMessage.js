/* eslint-disable */
import React from 'react';
import { useSubscription } from '@apollo/react-hooks';

import { SUBSCRIBE_CHAT_NEW_MESSAGE } from 'graphql/subscriptions';
import css from 'styles/modules/chat/Other.scss';

const ring = new Audio('//api.avoo.com.ua/uploads/chat/sound.mp3');
ring.volume = 0.2;

const UserLastMessage = ({ user, chatItem, selectedChat }) => {
  const { data } = useSubscription(SUBSCRIBE_CHAT_NEW_MESSAGE, {
    variables: {
      chatId: chatItem._id,
      receiver: chatItem.user._id,
    },
  });

  return (
    <p style={{ color: selectedChat._id === chatItem._id ? '#fff' : '#555' }} className={css.lastMessageDialog}>
      {data ? (data.chatNewMessage.message.slice(0, 80)) : chatItem.last_message.message && chatItem.last_message.message.slice(0, 80)}
    </p>
  );
};

export default UserLastMessage;
