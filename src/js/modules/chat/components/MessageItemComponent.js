/*  eslint-disable */ 
import React from 'react';
import cx from 'classnames';
import moment from 'moment';
import { Icon } from '@material-ui/core';
import { useSubscription } from '@apollo/react-hooks';

import { SUBSCRIBE_READ_MESSAGES } from 'graphql/subscriptions';

import css from 'styles/modules/chat/ChatMessage.scss';

const WithLinks = ({ reg, children }) => {
  var res = []
  children && children.replace(/((?:https?:\/\/|ftps?:\/\/|\bwww\.)(?:(?![.,?!;:()]*(?:\s|$))[^\s]){2,})|(\n+|(?:(?!(?:https?:\/\/|ftp:\/\/|\bwww\.)(?:(?![.,?!;:()]*(?:\s|$))[^\s]){2,}).)+)/gim, (m, link, text) => {
    res.push(link
      ? <a target="_blank" href={(link[0]==="w" ? "//" : "") + link} key={res.length}>{link}</a>
    : text
    );
  });

  return res;
}

const WithImg = ({ children }) => {
  const reg = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
  const str = children.match(reg);
  const images = String(str).split(',')

  return (
    <>
      <WithLinks reg={reg}>{children}</WithLinks>
      <div style={{ display: 'grid', paddingTop: '.5rem' }}>
        {images.map((img, index) => (
          <div style={{ maxWidth: '32rem' }} key={index}>
            <img src={img} alt="" />
          </div>
        ))}
      </div>
    </>
  );
}

const MessageItemComponent = ({ isCurrentUser, currentUser, selectedChat, messageInfo: { is_read, createdAt, message } }) => {
  const { data: readMessagesData } = useSubscription(SUBSCRIBE_READ_MESSAGES, {
    variables: {
      chatId: selectedChat._id,
      receiver: selectedChat.user._id,
    },
  });

  const readMessages = readMessagesData && readMessagesData.readMessagesSubscription.is_read === null;

  return (
    <li
      className={cx(
        css.messages__item,
        css[`messages__item_${!isCurrentUser ? 'start' : 'end'}`],
      )}
    >
      <div className={css.messages__wrap}>
        {currentUser.profile_img
          ? <img className={css.messages__img} src={currentUser.profile_img} alt="" />
          : (
            <span className={cx(css.messages__img, css.messages__img_none)}>
              {currentUser.first_name[0]}
              {currentUser.last_name[0]}
            </span>
          )
        }
        <div className={css.messages__info}>
          <p className={css.messages__user}>
            {currentUser.first_name}
            {' '}
            {currentUser.last_name}
            {' '}
          </p>
          <p className={css.messages__text}>
            <span>
              {(/(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/i).test(message)
                  ?
                    <span style={{ maxWidth: '80rem', display: 'block' }}>
                      <WithImg>{message}</WithImg>
                      {/* <span style={{ display: 'block' }}>{message}</span> */}
                    </span>
                  : <WithLinks>{message}</WithLinks>
              }
              &nbsp;
              &nbsp;
              &nbsp;
            </span>

            <small className={css.messages__checktime}>
              <span className={css.messages__time}>
                {moment(createdAt).fromNow()}
              </span>
              {isCurrentUser && <Icon className={css.messages__check}>{is_read ? 'done_all' : (readMessages ? 'done_all' : 'check')}</Icon>}
            </small>
          </p>
        </div>
      </div>
    </li>
  );
}

export default MessageItemComponent;
