import React from 'react';
import cx from 'classnames';
import { Button, Icon, IconButton, MenuItem } from '@material-ui/core';

import Fetching from 'components/Fetching';
import { BOTS } from 'config';

import css from 'styles/modules/chat/ChatSettings.scss';

const ChatTabSettingsComponent = ({
  data: { getChatGroup }, user,handleAddContact, handleLogout, createChatLoading, contact,
}) => {
  return (
    <section className={css.chatSettings}>
      <header className={cx(css.chatGroup__link, css.chatSettings__header)}>
        {user.profile_img
          ? <img className={css.chatGroup__img} src={user.profile_img} alt="" />
          : (
            <span className={cx(css.chatGroup__img, css.chatGroup__img_none, css.chatGroup__imgnone)}>
              {user.first_name[0]}
              {user.last_name[0]}
            </span>
          )
        }
        <div className={cx(css.chatGroup__name, css.chatSettings__name)}>
          {user.first_name}
          {' '}
          {user.last_name}
          <p>
            {user.email}
          </p>
        </div>
      </header>

      <div className={css.chatSettings__content}>
        <h4>
          Add bots, they are kind and c.o.o.l
        </h4>

        <ul className={css.chatBots}>
          {BOTS.map((bot) => (
            <li
              key={bot.id}
              className={css.chatGroup__item}
            >
              <MenuItem className={css.chatGroup__link} onClick={handleAddContact(bot)}>
                {bot.image !== ''
                  ? <img className={css.chatGroup__img} src={bot.image} alt="" />
                  : (
                    <span className={cx(css.chatGroup__img, css.chatGroup__img_none, css.chatGroup__imgnone)}>
                      {bot.first_name[0]}
                      {bot.last_name[0]}
                    </span>
                  )
                }
                <div className={css.chatGroup__name}>
                  {bot.value}
                  {' '}
                  {'bot'}
                </div>
                <p className={css.chatGroup__arrow}>
                  {getChatGroup.some((item) => item.user._id === bot.id)
                    ? (
                      <IconButton className={css.chatGroup__arrowDelete}>
                        <Icon>delete</Icon>
                      </IconButton>
                      )
                    : (!(createChatLoading && contact.id === bot.id) && <Icon>send</Icon>)
                  }
                </p>
                {createChatLoading && contact.id === bot.id && (
                  <div className={css.chatGroup__fetch}>
                    <Fetching isFetching color="secondary" size={18} thickness={4} reset isHeight />
                  </div>
                )}
              </MenuItem>
            </li>
          ))}
        </ul>
        <h4>
          A query language for your API...
        </h4>
        <div style={{ padding: '0 1.5rem', color: '#777' }}>
          <p>
            A query language for your API
            GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data.
          </p>
        </div>
        <h4>We are waiting for you!</h4>
        <div style={{ padding: '0 1rem 2rem .5rem' }}>
          <Button color="secondary" onClick={handleLogout}>
            Logout
          </Button>
        </div>
      </div>
    </section>

  );
};

export default ChatTabSettingsComponent;
