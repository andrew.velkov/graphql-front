/* eslint-disable */
import React from 'react';
import { useSubscription } from '@apollo/react-hooks';

import { SUBSCRIBE_USER_TYPPING } from 'graphql/subscriptions';
import LoadingImg from 'assets/images/loading.gif';

const UserTyping = ({ user, chatItem, children }) => {
  const { data } = useSubscription(SUBSCRIBE_USER_TYPPING, {
    variables: { receiver: chatItem.user._id },
  });
  // data && data.userTypingSubscription === user.id
  // chatItem._id == '5f1c4849e1fa0d09341427d1'
  return (
    <p style={{ lineHeight: '1.8rem', padding: 0, margin: '0' }}>
      {data && data.userTypingSubscription === user.id
      ? (
        <i style={{ paddingTop: '0', paddingBottom: 0, margin: '0' }}>
          <img width="50" src={LoadingImg} alt="" />
          typing
        </i>
      )
      : children}
    </p>
  );
};

export default UserTyping;
