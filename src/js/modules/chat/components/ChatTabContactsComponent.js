import React from 'react';
import cx from 'classnames';
import { Icon, MenuItem } from '@material-ui/core';

import Fetching from 'components/Fetching';
import Well from 'components/Well';

import css from 'styles/modules/chat/ChatTabContacts.scss';

const ChatTabContactsComponent = ({
  data: { getChatGroup }, usersFiltered, handleAddContact, createChatLoading, contact,
}) => {
  return (
    <ul className={cx(css.chatGroup, css.chatGroup_contacts)}>
      {usersFiltered.map((userItem) => (
        <li
          key={userItem.id}
          className={css.chatGroup__item}
        >
          <MenuItem className={css.chatGroup__link} onClick={handleAddContact(userItem)}>
            {userItem.profile_img
              ? <img className={css.chatGroup__img} src={userItem.profile_img} alt="" />
              : (
                <span className={cx(css.chatGroup__img, css.chatGroup__img_none, css.chatGroup__imgnone)}>
                  {userItem.first_name[0]}
                  {userItem.last_name[0]}
                </span>
              )
            }
            <div className={css.chatGroup__name}>
              {userItem.first_name}
              {' '}
              {userItem.last_name}
            </div>
            <p className={css.chatGroup__arrow}>
              {getChatGroup.some((item) => item.user._id === userItem.id)
                ? <Icon className={css.chatGroup__arrowDone}>done_all</Icon>
                : !(createChatLoading && contact.id === userItem.id) && <Icon>send</Icon>
              }
            </p>
            {createChatLoading && contact.id === userItem.id && (
              <div className={css.chatGroup__fetch}>
                <Fetching isFetching color="secondary" size={18} thickness={4} reset isHeight />
              </div>
            )}
          </MenuItem>
        </li>
      ))}
      {usersFiltered.length === 0 && <Well>No contacts...</Well>}
    </ul>
  );
};

export default ChatTabContactsComponent;
