import { combineReducers } from 'redux';

// other
import notification from './notification';
import authentication from './authentication';

const reducers = combineReducers({
  get: combineReducers({
    notifications: notification('notification'),
    authentication: authentication('authentication'),
  }),
});

export default reducers;
