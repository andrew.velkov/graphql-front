import { isEmpty } from 'lodash';

const initialState = {
  isAuth: false,
  isAuthToken: {},
};

export default function authentication(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `authentication/${params}/SUCCESS`:
        return {
          ...state,
          isAuth: !isEmpty(action.token.userId),
          isAuthToken: action.token,
        };
      default:
        return state;
    }
  };
}
