import { gql } from 'apollo-boost';

export const GET_USER_DATA = gql`
  query {
    user {
      id
      email
      first_name
      last_name
      last_seen
      profile_img
    }
    users {
      id
      email
      first_name
      last_name
      last_seen
      profile_img
    }
  }
`;

export const GET_CHAT_GROUP = gql`
  query($offset: Int) {
    getChatGroup(offset: $offset) {
      _id
      unread_messages,
      last_message {
        _id
        chat_id
        sender
        receiver
        is_read
        message
        createdAt
      }
      createdAt
      user {
        _id
        first_name
        last_name
        last_seen
        profile_img
      }
    }
  }
`;

export const GET_CHAT_MESSAGE = gql`
  query($chatId: ID, $offset: Int) {
    getChatMessage(chatId: $chatId, offset: $offset) {
      _id
      chat_id
      sender
      receiver
      message
      is_read
      createdAt
    }
  }
`;