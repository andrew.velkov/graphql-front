import { gql } from 'apollo-boost';

export const CREATE_CHAT_MESSAGE = gql`
  mutation($chatId: ID, $receiver: ID, $message: String) {
    createChatMessage(chatId: $chatId, receiver: $receiver, message: $message) {
      _id
    }
  }
`;

export const USER_TYPPING = gql`
  mutation($receiver: String) {
    userTyping(receiver: $receiver)
  }
`;

export const CREATE_CHAT = gql`
  mutation($receiver: ID) {
    createChat(receiver: $receiver) {
      _id
    }
  }
`;

export const READ_MESSAGES = gql`
  mutation($chatId: ID, $receiver: ID,) {
    readMessages(chatId: $chatId, receiver: $receiver) {
      _id
      is_read
    }
  }
`;
