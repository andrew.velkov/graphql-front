import { gql } from 'apollo-boost';

export const SUBSCRIBE_USER_TYPPING = gql`
  subscription ($receiver: String!) {
    userTypingSubscription(receiver: $receiver)
  }
`;

export const SUBSCRIBE_CHAT_NEW_MESSAGE = gql`
  subscription($chatId: ID, $receiver: ID) {
    chatNewMessage(chatId: $chatId, receiver: $receiver) {
      _id
      chat_id
      sender
      is_read
      receiver
      message
      createdAt
    }
  }
`;

export const SUBSCRIBE_READ_MESSAGES = gql`
  subscription($chatId: ID, $receiver: ID) {
    readMessagesSubscription(chatId: $chatId, receiver: $receiver) {
      _id
      is_read
    }
  }
`;
