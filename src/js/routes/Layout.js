import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import AuthLayout from 'components/Auth';

const Layout = ({ isAuth, children, ...props }) => (
  <>
    {isAuth ? (
      <>
        {children}
      </>
    ) : (
      <AuthLayout {...props}>{children}</AuthLayout>
    )}
  </>
);

export const PublicRoute = ({ component: Components, isAuth, ...rest }) => (
  <>
    <Route
      {...rest}
      render={(props) => (!isAuth ? <Components {...props} /> : <Redirect to="/chat" />)}
    />
  </>
);

export const PrivateRoute = ({ component: Components, isAuth, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (isAuth ? <Components {...props} /> : <Redirect exact to="/login" />)}
  />
);

export default Layout;

