/* eslint-disable */
import React, { Suspense, lazy, useEffect, useState } from 'react';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import jwt from 'jsonwebtoken';

import { isLoggedIn } from 'actions';
import { langLoad } from 'translations';
import Loader from 'components/Loader';
import ErrorBoundary from 'helpers/ErrorBoundary';
import Layout, { PrivateRoute, PublicRoute } from 'routes/Layout';

const App = lazy(() => import('components/App'));
const Registration = lazy(() => import('pages/Register'));
const Login = lazy(() => import('pages/Login'));
const ChatPage = lazy(() => import('pages/ChatPage'));

const LOGIN = gql`
  mutation($email: String, $password: String) {
    login(email: $email, password: $password) {
      accessToken
      refreshToken
    }
  }
`;

const REGISTER = gql`
  mutation($email: String!, $password: String!, $firstName: String, $lastName: String) {
    register(email: $email, password: $password, firstName: $firstName, lastName: $lastName) {
      accessToken
      refreshToken
    }
  }
`;

const Routes = ({ client }) => {
  const [login, { loading }] = useMutation(LOGIN);
  const [register, { loading: registerLoading }] = useMutation(REGISTER);
  const dispatch = useDispatch();
  const { isAuth } = useSelector((state) => state.get.authentication);

  const logout = () => {
    client.clearStore();
    localStorage.clear();
    dispatch(isLoggedIn(false));
  };

  const isAuthUser = () => {
    const tokens = JSON.parse(localStorage.getItem('token'));
    if (!tokens) {
      logout();
      return false;
    };

    const refreshTokenExpires = tokens && jwt.decode(tokens.refreshToken);
    const { exp, userId } = tokens && jwt.decode(tokens.accessToken);
    const getTime = new Date().getTime() / 1000;
    // auto-refresh token is coming soon!
    if (tokens && (refreshTokenExpires.exp <= getTime || exp <= getTime)) {
      logout();
      return false;
    };

    if ((isAuth || tokens) && userId) {
      const { accessToken, refreshToken } = tokens;
      dispatch(isLoggedIn({ userId, accessToken, refreshToken }));
    } else {
      logout();
      return false;
    }
  }

  useEffect(() => {
    langLoad();
    isAuthUser();
  }, []);

  const result = (res, value) => {
    if (res.errors) dispatch(isLoggedIn(false));
    const { accessToken, refreshToken } = res.data[value];
    const { userId } = jwt.decode(accessToken);
    if (!userId) return false;
    client.resetStore();
    localStorage.setItem('token', JSON.stringify({ accessToken, refreshToken }));
    dispatch(isLoggedIn({ userId, accessToken, refreshToken }));
  }

  const handleLogin = ({ email, password }) => {
    login({
      variables: {
        email,
        password,
      },
    }).then((res) => res && result(res, 'login'));
  }

  const handleRegister = ({ email, password, first_name, last_name }) => {
    register({
      variables: {
        email,
        password,
        firstName: first_name,
        lastName: last_name,
      },
    }).then((res) => res && result(res, 'register'));
  }

  return (
    <BrowserRouter>
      <Suspense fallback={<Loader isFetching />}>
        <App>
          <ErrorBoundary>
            <Layout isAuth={isAuth}>
              <Switch>
                {/* @PublicRoutes */}
                <PublicRoute
                  exect
                  path="/login"
                  isAuth={isAuth}
                  component={(props) => <Login data={{ handleLogin, loading } } {...props} />}
                />
                <PublicRoute
                  exect
                  path="/register"
                  isAuth={isAuth}
                  component={() => <Registration data={{ handleRegister, registerLoading }} />}
                />
                {/* forgot /reset pass... */}

                {/* @PrivateRoutes */}
                <PrivateRoute exect path="/chat" isAuth={isAuth} component={ChatPage} />

                <Redirect exect to="/login" />
              </Switch>
            </Layout>
          </ErrorBoundary>
        </App>
      </Suspense>
    </BrowserRouter>
  );
};

export default Routes;
