/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';

import strings from 'translations';
import Input from 'components/Form/Input';
import { SignupSchema } from 'helpers/Formik/validation';
import Formik from 'helpers/Formik';

import css from 'styles/components/Auth.scss';

const Register = ({ data: { handleRegister, registerLoading } }) => (
  <React.Fragment>
    <div className={css.auth__content}>
      <h2 className={css.auth__title}>{strings.other.registration}</h2>
      <Formik
        initialValues={{
          email: '',
          first_name: '',
          last_name: '',
          password: '',
          confirmPassword: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={handleRegister}
      >
        {({ values, errors, touched, handleChange, handleSubmit, isSubmitting }) => (
          <form className={css.auth__form}>
            <Input
              className={css.auth__form_input}
              label="First Name"
              variant="outlined"
              name="first_name"
              error={errors.first_name && touched.first_name}
              errorText={errors.first_name && touched.first_name && errors.first_name}
              value={values.first_name}
              onChange={handleChange}
            />
            <Input
              className={css.auth__form_input}
              label="Last Name"
              variant="outlined"
              name="last_name"
              error={errors.last_name && touched.last_name}
              errorText={errors.last_name && touched.last_name && errors.last_name}
              value={values.last_name}
              onChange={handleChange}
            />
            <Input
              className={css.auth__form_input}
              label={`${strings.form.email} *`}
              variant="outlined"
              type="email"
              name="email"
              error={errors.email && touched.email}
              errorText={errors.email && touched.email && errors.email}
              value={values.email}
              onChange={handleChange}
            />
            <Input
              className={css.auth__form_input}
              label={`${strings.form.password} *`}
              variant="outlined"
              type="password"
              name="password"
              error={errors.password && touched.password}
              errorText={errors.password && touched.password && errors.password}
              value={values.password}
              onChange={handleChange}
            />
            <Input
              className={css.auth__form_input}
              label={`${strings.form.password_confirm} *`}
              variant="outlined"
              type="password"
              name="confirmPassword"
              error={errors.confirmPassword && touched.confirmPassword}
              errorText={
                errors.confirmPassword && touched.confirmPassword && errors.confirmPassword
              }
              value={values.confirmPassword}
              onChange={handleChange}
            />
            <Button
              type="submit"
              color="primary"
              variant="contained"
              className={css.auth__button}
              disabled={isSubmitting || registerLoading}
              onClick={handleSubmit}
            >
              <span className={css.auth__button_icon} />
              {strings.buttons.register}
            </Button>
          </form>
        )}
      </Formik>

      <Link to="/login">{strings.buttons.login}</Link>
    </div>
  </React.Fragment>
);

export default Register;
