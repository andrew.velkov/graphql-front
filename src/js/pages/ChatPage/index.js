import React from 'react';
import ChatContainer from 'modules/chat';

const ChatPage = ({ ...props }) => <ChatContainer {...props} />;

export default ChatPage;
