import React from 'react';

import strings from 'translations';

import css from 'styles/components/Well.scss';

const Well = ({ children }) => (
  <p className={css.well}>
    <span>{children || strings.info.no_data}</span>
  </p>
);

export default Well;
