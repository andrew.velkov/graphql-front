import React from 'react';

const Member = ({ children }) => (
  <section>
    {children}
  </section>
);

export default Member;
