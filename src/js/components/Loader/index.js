import React from 'react';
import css from 'styles/components/Loader.scss';

import LoadingImg from 'assets/images/loading.gif';

const Loader = ({ isFetching = false }) => {
  if (isFetching) {
    return (
      <div className={css.loader}>
        {/* <div className={css.loader__circle} /> */}
        <div className={css.loader__line}>
          <img src={LoadingImg} alt="" />
        </div>
      </div>
    );
  }
  return null;
};

export default Loader;
