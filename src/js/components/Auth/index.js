/* eslint-disable */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { LinearProgress, withStyles } from '@material-ui/core';

import css from 'styles/components/Auth.scss';

const styles = {
  linearColorPrimary: {
    backgroundColor: '#f5f5f5',
  },
  linearBarColorPrimary: {
    backgroundColor: '#4e91d9',
  },
};

class Auth extends Component {
  loadingAuth = () => {
    const { sendRegister, sendLogin } = this.props;
    return sendRegister.loading || sendLogin.loading;
  };

  render() {
    const { classes, children } = this.props;

    return (
      <section className={css.auth}>
        <div className={css.auth__wrap}>
          <div className={css.auth__container}>
            {/* <LinearProgress
              classes={{
                colorPrimary: classes.linearColorPrimary,
                barColorPrimary: classes.linearBarColorPrimary,
              }}
              className={css.auth__progress}
            /> */}
            {children}
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(withStyles(styles)(Auth));
