import React from 'react';

// import Notifications from 'components/Notifications';


import css from 'styles/components/App.scss';

const App = ({ children }) => (
  <section className={css.app}>
    <div className={css.app__content}>
      {/* <Notifications /> */}
      {children}
    </div>
  </section>
);

export default App;
