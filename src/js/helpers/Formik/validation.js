import * as Yup from 'yup';

import strings from 'translations';

export const SignupSchema = () =>
  Yup.object().shape({
    email: Yup.string()
      .email(strings.validation.invalid_email)
      .required(strings.validation.required),
    password: Yup.string()
      .min(8, strings.formatString(strings.validation.min, { num: 8 }))
      .max(100, strings.formatString(strings.validation.max, { num: 100 }))
      .required(strings.validation.required),
    confirmPassword: Yup.string()
      .min(8, strings.formatString(strings.validation.min, { num: 8 }))
      .max(100, strings.formatString(strings.validation.max, { num: 100 }))
      .oneOf([Yup.ref('password'), null], strings.validation.password_not_match)
      .required(strings.validation.required),
    first_name: Yup.string()
      .min(3, strings.formatString(strings.validation.min, { num: 3 }))
      .max(100, strings.formatString(strings.validation.max, { num: 100 }))
      .required(strings.validation.required),
    last_name: Yup.string().max(100, strings.formatString(strings.validation.max, { num: 100 })),
  });

export const LoginSchema = () =>
  Yup.object().shape({
    email: Yup.string()
      .email(strings.validation.invalid_email)
      .required(strings.validation.required),
    password: Yup.string().required(strings.validation.required),
  });
