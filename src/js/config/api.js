const API = {
  // baseUrl: 'http://localhost:5000/api',
  baseUrl: 'https://api.avoo.com.ua/api',
  // baseUrl: 'http://134.209.88.98:5002/api',
  redirect_uri: window.location.origin,
};

export default API;
