import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { setContext } from 'apollo-link-context';

import { ApolloProvider } from '@apollo/react-hooks';
import { ApolloClient, InMemoryCache, split, HttpLink } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/link-ws';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import Routes from 'routes';
import store from 'redux/store';

import 'styles/main.scss';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    htmlFontSize: 10,
    fontSize: 14,
  },
  palette: {
    primary: {
      main: '#613EEA',
      contrastText: '#fff',
    },
    secondary: {
      main: '#f2075b',
      contrastText: '#fff',
    },
  },
});

const httpLink = new HttpLink({
  // uri: 'https://api.avoo.com.ua/graphql',
  uri: 'http://localhost:5000/graphql',
});

const wsLink = new WebSocketLink({
  uri: `ws://localhost:5000/graphql`,
  // uri: `wss://api.avoo.com.ua/graphql`,
  options: {
    lazy: true,
    reconnect: true,
    connectionParams: () => {
      const tokens = JSON.parse(localStorage.getItem('token'));
      return {
        headers: {
          Authorization: tokens ? `Bearer ${tokens.accessToken}` : '',
        },
      };
    },
  },
});

const authLink = setContext(() => {
  const tokens = JSON.parse(localStorage.getItem('token'));
  return {
    headers: {
      Authorization: tokens ? `Bearer ${tokens.accessToken}` : '',
    },
  };
});

const link = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
  },
  wsLink,
  authLink.concat(httpLink),
);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <Routes client={client} />
      </MuiThemeProvider>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root'),
);
